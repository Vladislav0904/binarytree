#include <iostream>
#include "BinarySearchTree.h"
#include "BinarySearchTree.cpp"

int main() {
	BinarySearchTree<int> tree;
	std::cout << "Insterting 1 in Tree and using iterativeSearch to find it: ";
	tree.insert(1);
	std::cout << tree.iterativeSearch(1) << "\n";
	std::cout << "Deleting 1 in Tree and using iterativeSearch to find it: ";
	tree.deleteKey(1);
	std::cout << tree.iterativeSearch(1) << "\n";
	std::cout << "Adding some more elems" << "\n";
	tree.insert(5);
	tree.insert(3);
	tree.insert(7);
	tree.insert(1);
	tree.insert(4);
	tree.insert(6);
	tree.insert(8);
	std::cout << "Get Nodes count: ";
	std::cout << tree.getCount() << "\n";
	std::cout << "Get Height: ";
	std::cout << tree.getHeight() << "\n";
	std::cout << "Printing tree: ";
	tree.print(std::cout);
	std::cout << "\n";
	std::cout << "Using iterative in order walk: ";
	tree.iterativeInorderWalk();
	std::cout << "\n";
	std::cout << "Using recursive in order walk: ";
	tree.inorderWalk();
	std::cout << "\n";
	std::cout << "Walking the tree by levels: ";
	tree.walkByLevels();
	std::cout << "\n";
	std::cout << "Created new tree with same elements" << "\n";
	BinarySearchTree<int> tree2;
	tree2.insert(5);
	tree2.insert(3);
	tree2.insert(7);
	tree2.insert(1);
	tree2.insert(4);
	tree2.insert(6);
	tree2.insert(8);
	std::cout << "Checking if trees are similar: ";
	std::cout << tree.isSimilar(tree2) << "\n";
	std::cout << "Checking if there is any similar key: ";
	std::cout << tree.isIdenticalKey(tree2) << "\n";
	std::cout << "Deleting one elem" << "\n";
	tree2.deleteKey(5);
	std::cout << "Checking if trees are similar (they are different now): ";
	std::cout << tree.isSimilar(tree2) << "\n";
	std::cout << "Checking if there is any similar key (There are still some similar keys): ";
	std::cout << tree.isIdenticalKey(tree2) << "\n";
	return 0;
}