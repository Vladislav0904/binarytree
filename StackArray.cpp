#pragma once
#include "StackArray.h"
#include "Stack.h"

template <class T>
void StackArray<T>::swap(StackArray<T>& src)
{
	std::swap(this->array_, src.array_);
	std::swap(top_, src.top_);
	std::swap(size_, src.size_);
}

template <class T>
void StackArray<T>::push(const T& e)
{
	if (top_ == size_) {
		throw StackOverflow();
	}
	array_[++top_] = e;
}


template <class T>
StackArray<T>::StackArray(size_t size) :
	size_(size),
	top_(0)
{
	try {
		array_ = new T[size + 1]; 
	}
	catch (...) { 
		throw WrongStackSize();
	}
}

template <class T>
T& StackArray<T>::pop()
{
	if (isEmpty()) {
		throw StackUnderflow();
	}
	return array_[top_--]; 
}
