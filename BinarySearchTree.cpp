#pragma once
#include <iostream>
#include "StackArray.h"
#include "StackArray.cpp"
#include "BinarySearchTree.h"
#include <queue>

template <class T>
BinarySearchTree<T>::BinarySearchTree() : root_(nullptr) {};

template <class T>
BinarySearchTree<T>::BinarySearchTree(BinarySearchTree&& other) noexcept : root_(other.root_) {
    other.root_ = nullptr;
}

template<class T>
BinarySearchTree<T>& BinarySearchTree<T>::operator=(BinarySearchTree<T>&& src) noexcept
{
    if (this != &src) {
        delete root_;
        root_ = src.root_;
        src.root_ = nullptr;
    }
    return *this;
}

template<class T>
BinarySearchTree<T>::~BinarySearchTree()
{
    delete root_;
}


template<class T>
bool BinarySearchTree<T>::iterativeSearch(const T& key) const
{
    Node<T>* current = iterativeSearchNode(key);
    return (current != nullptr); 
}

template<class T>
bool BinarySearchTree<T>::insert(const T& key) {
    Node<T>* curr = root_;
    Node<T>* parent = nullptr;

    while (curr != nullptr) {
        if (key == curr->key_) {
            return false;
        }
        parent = curr;
        curr = (key < curr->key_) ? curr->left_ : curr->right_;
    }
    Node<T>* newNode = new Node<T>(key);

    if (parent == nullptr) {
        root_ = newNode;
    }
    else if (key < parent->key_) {
        parent->left_ = newNode;
    }
    else {
        parent->right_ = newNode;
    }

    return true;
}

template <typename T>
bool BinarySearchTree<T>::deleteKey(const T& key) {
    Node<T>* parent = nullptr;
    Node<T>* current = root_;

    current = iterativeSearchNode(key);
    if (current == nullptr) {
        return false;
    }

    // no parents
    if (current->left_ == nullptr && current->right_ == nullptr) {
        if (current == root_) {
            root_ = nullptr;
        }
        else if (current == parent->left_) {
            parent->left_ = nullptr;
        }
        else {
            parent->right_ = nullptr;
        }
        delete current;
    }
    // one parent
    else if (current->left_ == nullptr || current->right_ == nullptr) {
        Node<T>* child;
        if (current->left_ != nullptr) {
            child = current->left_;
        }
        else {
            child = current->right_;
        }

        if (current == root_) {
            root_ = child;
        }
        else if (current == parent->left_) {
            parent->left_ = child;
        }
        else {
            parent->right_ = child;
        }
        delete current;
    }
    // two parents
    else {
        Node<T>* successor = current->right_;
        Node<T>* successorParent = current;

        while (successor->left_ != nullptr) {
            successorParent = successor;
            successor = successor->left_;
        }

        current->key_ = successor->key_;
        if (successor == successorParent->left_) {
            successorParent->left_ = successor->right_;
        }
        else {
            successorParent->right_ = successor->right_;
        }
        delete successor;
    }
    return true;
}


template <typename T>
void BinarySearchTree<T>::print(std::ostream& out) const {
    printNode(out, root_);
}

template <typename T>
void BinarySearchTree<T>::printNode(std::ostream& out, Node<T>* root) const {
    if (root == nullptr) {
        return;
    }
    out << "(" << root->key_;
    if (root->left_ != nullptr || root_->right_ != nullptr) {
        out << " ";
        printNode(out, root->left_);
        out << " ";
        printNode(out, root->right_);
    }
    out << ")";
}
template <typename T>
int BinarySearchTree<T>::getCount(const Node<T>* node) const
{
    if (node == nullptr) {
        return 0;
    }
    return (1 + getCount(node->left_) + getCount(node->right_));
}


template <typename T>
int BinarySearchTree<T>::getCount() const {
    return getCount(this->root_);
}

template<typename T>
int BinarySearchTree<T>::getHeight(const Node<T>* node) const {
    if (node == nullptr) {
        return 0;
    }
    else {
        int left_height = getHeight(node->left_);
        int right_height = getHeight(node->right_);
        return std::max(left_height, right_height) + 1;
    }
}

template<typename T>
int BinarySearchTree<T>::getHeight() const {
    return getHeight(root_);
}

template<typename T>
void BinarySearchTree<T>::iterativeInorderWalk() const
{
    StackArray<Node<T>*> stack;
    Node<T>* current = root_;

    while (current || !stack.isEmpty()) {
        while (current) {
            stack.push(current);
            current = current->left_;
        }

        current = stack.top();
        stack.pop();
        std::cout << current->key_ << " ";

        current = current->right_;
    }
}


template<typename T>
void BinarySearchTree<T>::inorderWalk(Node<T>* node) const {
    if (node == nullptr) {
        return;
    }
    inorderWalk(node->left_);
    std::cout << node->key_ << " ";
    inorderWalk(node->right_);
}

template <typename T>
void BinarySearchTree<T>::inorderWalk() const {
    inorderWalk(root_);
}

template <typename T>
void BinarySearchTree<T>::walkByLevels() const {
    if (root_ == nullptr) {
        return;
    }
    std::queue<Node<T>*> q;
    q.push(root_);
    while (!q.empty()) {
        Node<T>* curr = q.front();
        q.pop();
        std::cout << curr->key_ << " ";
        if (curr->left_ != nullptr) {
            q.push(curr->left_);
        }
        if (curr->right_ != nullptr) {
            q.push(curr->right_);
        }
    }
}

template<typename T>
bool BinarySearchTree<T>::isSimilar(const BinarySearchTree<T>& other) const {
    Node<T>* thisNode = root_;
    Node<T>* otherNode = other.root_;

    StackArray<Node<T>*> thisStack;
    StackArray<Node<T>*> otherStack;

    while (thisNode != nullptr || !thisStack.isEmpty() || otherNode != nullptr || !otherStack.isEmpty()) {
        if (thisNode != nullptr || otherNode != nullptr) {
            if (thisNode != nullptr && otherNode != nullptr) {
                if (thisNode->key_ != otherNode->key_) {
                    return false;
                }
                thisStack.push(thisNode);
                otherStack.push(otherNode);
                thisNode = thisNode->left_;
                otherNode = otherNode->left_;
            }
            else {
                return false;
            }
        }
        else {
            thisNode = thisStack.top();
            thisStack.pop();
            otherNode = otherStack.top();
            otherStack.pop();
            thisNode = thisNode->right_;
            otherNode = otherNode->right_;
        }
    }

    return true;
}

template<typename T>
bool BinarySearchTree<T>::isIdenticalKey(const BinarySearchTree<T>& other) const {
    Node<T>* current1 = root_;
    Node<T>* current2 = other.root_;

    StackArray<Node<T>*> stack1, stack2;
    while (current1 != nullptr || !stack1.isEmpty() || current2 != nullptr || !stack2.isEmpty()) {
        while (current1 != nullptr) {
            stack1.push(current1);
            current1 = current1->left_;
        }
        while (current2 != nullptr) {
            stack2.push(current2);
            current2 = current2->left_;
        }
        if (stack1.isEmpty() || stack2.isEmpty()) {
            return false;
        }
        current1 = stack1.top();
        current2 = stack2.top();
        if (current1->key_ == current2->key_) {
            return true;
        }
        if (current1->key_ < current2->key_) {
            stack1.pop();
            current1 = current1->right_;
            current2 = nullptr;
        }
        else {
            stack2.pop();
            current2 = current2->right_;
            current1 = nullptr;
        }
    }
    return false;
}